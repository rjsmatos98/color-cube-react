import styled from "styled-components";

export const SearchInput = styled.input`
    width: 100%;
    height: 22px;
    border: 1px solid #c7c7c7c7;
    border-radius: 2px;
    outline: none;
    padding: 5px;
`;