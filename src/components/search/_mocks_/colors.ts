import { reduce } from "lodash";

export const colors =[
    {
        name: "Red",
        value: "#F00"
    },
    {
        name: "Blue",
        value: [0,0,255]
    },
    {
        name: "Dark blue",
        value: "#00008B"
    }
]