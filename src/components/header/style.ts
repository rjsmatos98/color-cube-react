import styled from "styled-components";

const getRGB = (colors: Array<number>) => colors.join(", ");

export const HeaderStyled = styled.header<{
    bgColor: Array<number>
}>`
    background-color: rgb(${({bgColor}) => getRGB(bgColor)});
    text-align: center;
    padding: 5px;
    border-bottom: 1px solid #c7c7c764;
`;

export const HeaderTitle = styled.h1<{
    titleColor: Array<number>
}>`
    color: rgb(${({titleColor}) => getRGB(titleColor)});
    text-shadow: 4px 2px 15px black;
`;