import React, { useState } from "react";
import ColorTile from "./components/color-tile"
import HeaderComp from "./components/header";
import Search from "./components/search"
import { ColorWrapper } from "./style";
import { Color } from "./types/color"

const App = () => {
  const [colors, setColors] = useState<Array<Color>>([])
  return (
    <>
      <HeaderComp />
      <Search onSearch={(colors) => setColors(colors)}/>
      <ColorWrapper>
        {colors.map((color, i) => (
          <ColorTile {...color} key={ color.name} />
        ))}
      </ColorWrapper>
    </>
  )
}

export default App;